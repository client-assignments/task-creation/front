# Task Management Frontend

## Introduction

- Purpose of project
  - A Task Management Frontend to send Task data to server .

#### 1.1 Module features

- Create_task: You can create task by filling appropriate details of form and hit save or save_&_add_task or save_&_add_Pitch/Hit
- Task_table: All task list displayed here so easily watch out all task 

#### 1.3 Module technical stack

- **Angular Version** : 14.0.0
- **Angular material** : 14.1.0
- **Type script** : ~4.7.2

#### 1.4 Installation & Configuration (Local)

follow the steps below to setup project

1. Install dependencies via NPM

```bash
npm i # on local machine
```

```bash
npm i --only=prod # on server
```

2. Change environment variables in `src/environments/` directory
3. Change socket connection URL in `src/app/app.module.ts` in `config` variable
4. Just execute

```bash
npm start
```

Project will up on local machine.

#### 1.5 Get up and running

- Install requirements

  - Open the terminal from root folder of this repository
  - Run `npm i` to install all the dependencies
  - Currently, the frontend server will start on PORT 4200, which is configurable

#### 1.6 Folder Structure

Common structure that is used in this project is as following

```
.
└── src
    └── app
            ├── app.route.ts
            ├── app.module.ts
        └── user-detail-table
            ├── user-detail-table.component.html
            ├── user-detail-table.component.css
            ├── user-detail-table.component.spec.ts
            ├── user-detail-table.component.ts
        └── form
            ├── form.component.html
            ├── form.component.css
            ├── form.component.spec.ts
            ├── form.component.ts
```


