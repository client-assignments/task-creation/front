import { Component, OnInit } from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {FormComponent} from "../../components/form/form.component";
import { DataService } from '../../service/data.service';

import {CommonService} from "../../service/common.service";

@Component({
  selector: 'app-user-detail-table',
  templateUrl: './task-detail-table.component.html',
  styleUrls: ['./task-detail-table.component.scss'],
})
export class TaskDetailTableComponent implements OnInit {
  taskList: Array<{
    tasktitle: string,
    project: string,
    client: string,
    assignee:string,
    status:string,
    priority:string,
    duedate:string,
    type:string,
    createdby:string
  }> = [
  ];
  columns = [
    {header: 'Task Title' , column: 'tasktitle'},
    {header: 'Project', column: 'project'},
    {header: 'Client', column: 'client'},
    {header: 'Assignee', column: 'assignee'},
    {header: 'Status',column: 'status'},
    {header: 'Priority',column: 'priority'},
    {header: 'Due Date',column: 'duedate'},
    {header: 'Type', column: 'type'},
    {header: 'Created By',column: 'createdby'}
  ];
  displayedColumns: string[] = [];
  constructor(public dialog: MatDialog, private dataGetService: DataService, private  commonSerivce:CommonService) { }

  ngOnInit(): void {
    this.displayedColumns = this.displayedColumns.concat(this.columns.map(x => x.column));
    this.commonSerivce.task.subscribe(()=>{
      this.getTaskList();
    });
    this.getTaskList();
  }

  getTaskList(){
    this.dataGetService.getTaskList().subscribe(
      (res)=>{
        let newTask = [];
        for(let task of res){
          newTask.push({
            tasktitle: task.taskTitle,
            project: task.project.name,
            client: task.client.name,
            assignee: task.assignee.name,
            status: task.status.name,
            priority: task.priority.name,
            duedate: task.dueDate.toString().split('T')[0],
            type: task.type.name,
            createdby: task.createdBy.name
          });
        }
        this.taskList = [...newTask];
      }
    )
  }

  openDialog(): void {
    this.dialog.open(FormComponent, {
      width: '800px',
      height: '650px',
      panelClass:'formdialog'
    });
  }

}
