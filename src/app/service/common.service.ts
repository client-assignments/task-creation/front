import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CommonService {
  task = new Subject<void>;
  selectedProject = new Subject<any>();
  constructor() {}
}
