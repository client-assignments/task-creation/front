import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ITask } from '../interfaces/ITask';
import { IUsers } from '../interfaces/IUsers';
import { IGeneric } from '../interfaces/IGeneric';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  constructor(private http: HttpClient) {}

  getTaskList(): Observable<Array<ITask>> {
    return this.http.get<Array<ITask>>('https://demo.task-management.gradlesol.com/app/tasks');
  }

  getClients(): Observable<Array<IUsers>> {
    return this.http.get<Array<IUsers>>('https://demo.task-management.gradlesol.com/app/clients');
  }

  getStatuses(): Observable<Array<IGeneric>> {
    return this.http.get<Array<IGeneric>>('https://demo.task-management.gradlesol.com/app/statuses');
  }

  getTaskTypes(): Observable<Array<IGeneric>> {
    return this.http.get<Array<IGeneric>>('https://demo.task-management.gradlesol.com/app/types');
  }

  getPriorities(): Observable<Array<IGeneric>> {
    return this.http.get<Array<IGeneric>>('https://demo.task-management.gradlesol.com/app/priorities');
  }

  getUsers(): Observable<Array<IUsers>> {
    return this.http.get<Array<IUsers>>('https://demo.task-management.gradlesol.com/app/users');
  }

  getProjects(): Observable<Array<IGeneric>> {
    return this.http.get<Array<IGeneric>>('https://demo.task-management.gradlesol.com/app/projects');
  }
  createTask(taskDetails: ITask) {
    return this.http.post('https://demo.task-management.gradlesol.com/app/tasks', taskDetails);
  }
}
