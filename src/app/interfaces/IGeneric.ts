export interface IGeneric {
  id: string;
  name: string;
  createdAt?: Date;
  updatedAt?: Date;
  isArchived?: boolean;
}
