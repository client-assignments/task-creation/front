export interface Client {
  name: string;
}

export interface Status {
  name: string;
}

export interface Project {
  name: string;
}

export interface Priority {
  name: string;
}

export interface Type {
  name: string;
}

export interface Assignee {
  name: string;
}

export interface CreatedBy {
  name: string;
}
export interface ITask {
  id: string;
  dueDate: Date;
  clientId: string;
  projectId: string;
  statusId: string;
  typeId: string;
  priorityId: string;
  taskTitle: string;
  notes: string;
  emailNotes: string;
  projectDateFilter: boolean;
  createdAt: Date;
  updatedAt: Date;
  isArchived: boolean;
  assigneeId: string;
  createdById: string;
  client: Client;
  status: Status;
  project: Project;
  priority: Priority;
  type: Type;
  assignee: Assignee;
  createdBy: CreatedBy;
}
