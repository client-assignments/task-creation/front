import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ProjectDialogComponent } from '../project-dialog/project-dialog.component';
import { CommonService } from '../../service/common.service';
import { DataService } from '../../service/data.service';
import {ITask} from "../../interfaces/ITask";
import {IGeneric} from "../../interfaces/IGeneric";
import {MatSnackBar} from "@angular/material/snack-bar";
declare const $: any;

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  userDetailObject!: ITask;
  taskForm! : FormGroup;
  projectId:string='';
  projectName:string='';
  taskName:string='';
  users:Array<IGeneric>= [];
  clients:Array<IGeneric>= [];
  statuses:Array<IGeneric>=[];
  types:Array<IGeneric>=[];
  priorities:Array<IGeneric>=[];

  constructor(public dialog: MatDialog,
              private dataService : DataService,
              private commonService: CommonService,
              private _snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.initForm();
    this.commonService.selectedProject.subscribe((res)=>{
      if(this.projectName !== res.name){
        this.taskForm.controls['projectId'].setValue(res.id);
        this.projectName = res.name;
      }
    })
    this.dataService.getUsers().subscribe(
      (res)=>{
        this.users = res;
      }
    );
    this.dataService.getClients().subscribe(
      (res)=>{
        this.clients  =res;
      }
    );
    this.dataService.getStatuses().subscribe(
      (res)=>{
        this.statuses = res;
      }
    );
    this.dataService.getTaskTypes().subscribe(
      (res)=>{
        this.types = res;
      }
    );
    this.dataService.getPriorities().subscribe(
      (res)=>{
        this.priorities = res;
      }
    );

  }

  private initForm(){
    this.taskForm = new FormGroup({
      'dueDate': new FormControl('' , Validators.required),
      'createdById': new FormControl('', Validators.required),
      'clientId': new FormControl('', Validators.required),
      'projectId': new FormControl('', Validators.required),
      'taskTitle': new FormControl('', Validators.required),
      'statusId': new FormControl('', Validators.required),
      'assigneeId': new FormControl('', Validators.required),
      'sendEmail': new FormControl(false),
      'typeId': new FormControl('', Validators.required),
      'priorityId': new FormControl('', Validators.required),
      'notes':new FormControl(''),
      'emailNotes': new FormControl('')
    })
  }

  openProjectDailog(){
    this.dialog.open(ProjectDialogComponent, {
      width: '500px',
      height: '310px',
      panelClass:'formdialog'
    });
  }

  onSubmit(e?: Event) {
    if(!this.taskForm.invalid) {
      this.dataService.createTask(this.taskForm.value).subscribe({
        next: (res) => {
          this._snackBar.open('Task created successfully.' , '',{
            duration: 3000,
            panelClass: 'success'
          });
          if(!e){
            this.dialog.closeAll();
          }
          this.commonService.task.next();
        },
        error: () => {
          this._snackBar.open('Required fields are missing to create Task.', '',{
            duration: 3000,
            panelClass: 'error'
          });
        }
      });
      this.taskForm.reset();
      if(e){
        e.preventDefault();
      }
    } else {
      this._snackBar.open('Required fields are missing to create Task.', '',{
        duration: 3000,
        panelClass: 'error'
      });
    }

  }

}


