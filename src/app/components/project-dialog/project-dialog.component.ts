import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { CommonService } from '../../service/common.service';
import { DataService } from '../../service/data.service';

@Component({
  selector: 'app-project-dialog',
  templateUrl: './project-dialog.component.html',
  styleUrls: ['./project-dialog.component.scss'],
})
export class ProjectDialogComponent implements OnInit {
  projects: Array<{
    id: string;
    name: string;
  }> = [];
  constructor(private dataGetService: DataService, private commonService: CommonService) {}

  ngOnInit(): void {
    this.dataGetService.getProjects().subscribe((res: any) => {
      for (let project of res) {
        this.projects.push({ id: project.id, name: project.name });
      }
    });
  }

  onClick(id: string, name: string) {
    this.commonService.selectedProject.next({ id: id, name: name });
  }
}
